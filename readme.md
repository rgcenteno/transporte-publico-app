Gestión transporte ayuntamiento
Un ayuntamiento necesita gestionar su transporte público. El ayuntamiento tiene un nombre
de ayuntamiento (String) y además cuenta con una flota de autobuses y un servicio de metro.
Tanto los autobuses como los metros tienen un año de compra, una capacidad máxima y una
cadena única que lo identifica. Además, todos tienen un monedero con el dinero que recaudan
y el número de pasajeros que transportan actualmente.
Los autobuses tienen una ruta (string). El metro tiene asociado un color de línea (String) y si
abre los fines de semana por la noche o no.
El precio del billete de autobús es el mismo para todos los autobuses. El precio del billete de
metro es común para todos los metros.
Implementa:
1. Todos los transportesPublicos: subirPasajero() return boolean: Permite subir a un
pasajero si la capacidad no está completa. Suman el precio del billete a la caja del
transporte.
2. Todos los transportesPublicos: bajarPasajero() return boolean: Baja un pasajero si al
menos hay un pasajero en el autobús.
3. Modificar toString() en autobús para que muestre todos los datos del autobús.
4. Modificar toString() en metro para que muestre todos los datos del metro.
5. Modificar toString() en ayuntamiento para que muestre el nombre del ayuntamiento y
los datos de los transportes públicos que tiene.
6. Crear un método en ayuntamiento que permita añadir un transporte nuevo (return
boolean).
7. Crear un método en ayuntamiento que permita borrar un transporte por su
identificador.
8. Crear un método en ayuntamiento que muestre el total de dinero recaudado.
9. Crear un método en ayuntamiento que permita cambiar el precio del billete de todos
los buses.
10. Lo mismo para metros.
Para cada apartado crea en el main un método que lo testee. El método deberá llamarse:
Public static void test<numero_ejercicio>();
El método debe crear los objetos que se necesiten para testear la implement
