/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw1.transportepublicoapp.clases;

/**
 *
 * @author rgcenteno
 */
public class Metro extends MedioTransporte {
    private String colorLinea;
    private boolean finDeSemana;
    private static double precioBillete = 2.5;

    public Metro(String id, int anhoFabricacion, int capacidadMaxima, String colorLinea, boolean finDeSemana) {
        super(id, anhoFabricacion, capacidadMaxima);
        checkNotNullNotBlank(colorLinea);
        this.colorLinea = colorLinea;
        this.finDeSemana = finDeSemana;
    }

    public static double getPrecioBillete() {
        return precioBillete;
    }

    public static void setPrecioBillete(double precioBillete) {
        checkNumberGreaterThanZero(precioBillete);
        Metro.precioBillete = precioBillete;
    }

    @Override
    public boolean subirPasajero() {
        if(super.subirPasajero()){            
            this.incrementarMonedero(precioBillete);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public String toString() {
        return "Metro:\n" + super.toString() + "\nColor: " + colorLinea + "\nFin de semana: " + this.finDeSemana; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
