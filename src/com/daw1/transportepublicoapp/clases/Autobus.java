/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw1.transportepublicoapp.clases;

/**
 *
 * @author rgcenteno
 */
public class Autobus extends MedioTransporte{
    
    private String ruta;
    private static double precioBillete = 1.25;

    public Autobus(String id, int anhoFabricacion, int capacidadMaxima, String ruta) {
        super(id, anhoFabricacion, capacidadMaxima);
        checkNotNullNotBlank(ruta);
        this.ruta = ruta;
    }

    @Override
    public String toString() {
        return "Autobús: " + super.toString() + "\nRuta: " + this.ruta;
    }

    public static double getPrecioBillete() {
        return precioBillete;
    }

    public static void setPrecioBillete(double precioBillete) {
        checkNumberGreaterThanZero(precioBillete);
        Autobus.precioBillete = precioBillete;
    }

    @Override
    public boolean subirPasajero() {
        if(super.subirPasajero()){            
            this.incrementarMonedero(precioBillete);
            return true;
        }
        else{
            return false;
        }
    }
    
    
    
}
