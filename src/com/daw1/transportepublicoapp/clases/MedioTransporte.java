/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw1.transportepublicoapp.clases;

import com.google.common.base.Preconditions;
/**
 *
 * @author rgcenteno
 */
public abstract class MedioTransporte implements Comparable<MedioTransporte>{
    
    private String id;
    private int anhoFabricacion;
    private int capacidadMaxima;
    private double monedero;
    private int numPasajeros;

    protected MedioTransporte(String id, int anhoFabricacion, int capacidadMaxima) {
        checkNotNullNotBlank(id);
        checkAnhoFabricacion(anhoFabricacion);
        checkNumberGreaterThanZero(capacidadMaxima);
        this.id = id;
        this.anhoFabricacion = anhoFabricacion;
        this.capacidadMaxima = capacidadMaxima;
        this.monedero = 0;
        this.numPasajeros = 0;
    }
    
    protected static void checkNotNullNotBlank(String txt){
        Preconditions.checkNotNull(txt);
        Preconditions.checkArgument(!txt.isBlank());
    }
    
    private static void checkAnhoFabricacion(int anhoFabricacion){
        Preconditions.checkArgument(anhoFabricacion <= java.time.LocalDate.now().getYear());
    }
    
    protected static void checkNumberGreaterThanZero(Number n){
        Preconditions.checkArgument(n.doubleValue() > 0);
    }
        
    public boolean subirPasajero() {
        if(this.capacidadMaxima > this.numPasajeros){
            numPasajeros++;            
            return true;
        }
        else{
            return false;
        }
    }
    
    public final boolean bajarPasajero() {
        if(this.numPasajeros > 0){
            numPasajeros--;
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public String toString() {
        return "ID: " + id + "\n" + "Año fabricación: " + this.anhoFabricacion + "\nCapacidad máxima: " + this.capacidadMaxima + "\nMonedero: " + this.monedero + "€\nNúmero pasajeros: "+ this.numPasajeros;
    }

    @Override
    public int compareTo(MedioTransporte t) {
        return this.id.compareTo(t.id);
    }
    
    protected void incrementarMonedero(double cantidad){
        Preconditions.checkArgument(cantidad > 0, "Debe establecer una cantidad positiva a incrementar");
        monedero += cantidad;
    }

    public String getId() {
        return id;
    }

    public double getMonedero() {
        return monedero;
    }

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public int getNumPasajeros() {
        return numPasajeros;
    }
    
    
}
