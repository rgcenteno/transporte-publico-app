/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw1.transportepublicoapp.clases;

import com.google.common.base.Preconditions;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author rgcenteno
 */
public class Ayuntamiento {
    private final String nombre;
    
    private java.util.Map<String, MedioTransporte> transportes;

    public Ayuntamiento(String nombre) {        
        Preconditions.checkNotNull(nombre);
        Preconditions.checkArgument(!nombre.isBlank());
        this.nombre = nombre;
        this.transportes = new TreeMap<>();
    }       

    public String getNombre() {
        return nombre;
    }
    
    public boolean addTransporte(MedioTransporte mt){
        Preconditions.checkNotNull(mt);
        if(!transportes.containsKey(mt.getId())){
            transportes.put(mt.getId(), mt);
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean removeTransporte(String id){
        Preconditions.checkNotNull(id);
        if(transportes.containsKey(id)){
            transportes.remove(id);
            return true;
        }
        else{
            return false;
        }
    }
    
    public double getDineroRecaudado(){
        double total = 0;
        for(MedioTransporte mt : transportes.values()){
            total += mt.getMonedero();
        }
        return total;
    }
    
    public void setPrecioBilleteAutobus(double precio){
        Preconditions.checkArgument(precio > 0);
        Autobus.setPrecioBillete(precio);
    }
    
    public void setPrecioBilleteMetro(double precio){
        Preconditions.checkArgument(precio > 0);
        Metro.setPrecioBillete(precio);
    }

    public Map<String, MedioTransporte> getTransportes() {
        return java.util.Collections.unmodifiableMap(transportes);
    }

    @Override
    public String toString() {
        return "Ayuntamiento" + "nombre=" + nombre + ", transportes=" + transportes;
    }
    
    
}
