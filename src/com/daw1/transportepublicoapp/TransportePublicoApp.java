/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw1.transportepublicoapp;

import com.daw1.transportepublicoapp.clases.*;

/**
 *
 * @author rgcenteno
 */
public class TransportePublicoApp {

    private static Ayuntamiento ayuntamiento;
    public static void main(String[] args) {
        inicializar();
        ejercicio01();
        ejercicio02();
        ejercicio0304();
        ejercicio05();
        ejercicio06();
        ejercicio07();
        ejercicio0809();
        // TODO code application logic here
    }
    
    private static void inicializar(){
        ayuntamiento = new Ayuntamiento("Test");
        ayuntamiento.addTransporte(new Autobus("1", 2010, 10, "A-B"));
        ayuntamiento.addTransporte(new Autobus("2", 2015, 15, "C-B"));
        ayuntamiento.addTransporte(new Autobus("3", 2020, 20, "F-Q"));
        
        ayuntamiento.addTransporte(new Metro("4", 2000, 20, "Amarillo", false));
        ayuntamiento.addTransporte(new Metro("5", 2002, 20, "Verde", false));
        ayuntamiento.addTransporte(new Metro("6", 2003, 25, "Violeta", true));
    }
    
    public static void ejercicio01(){
        for(int i = 0; i < 30; i++){
            for(MedioTransporte mt : ayuntamiento.getTransportes().values()){
                if(mt.subirPasajero()){
                    System.out.println("Sube pasajero: " + mt.getNumPasajeros() + " / " + mt.getCapacidadMaxima()); 
                }
                else{
                    System.out.println("NO SUBE pasajero: " + mt.getNumPasajeros() + " / " + mt.getCapacidadMaxima()); 
                }
            }
        }
    }
    
    public static void ejercicio02(){
        for(int i = 0; i < 30; i++){
            for(MedioTransporte mt : ayuntamiento.getTransportes().values()){
                if(mt.bajarPasajero()){
                    System.out.println("Baja pasajero: " + mt.getNumPasajeros() + " / " + mt.getCapacidadMaxima()); 
                }
                else{
                    System.out.println("NO BAJA pasajero: " + mt.getNumPasajeros() + " / " + mt.getCapacidadMaxima()); 
                }
            }
        }
    }
    
    public static void ejercicio0304(){        
        for(MedioTransporte mt : ayuntamiento.getTransportes().values()){
            System.out.println(mt);
        }        
    }
    
    public static void ejercicio05(){        
        System.out.println(ayuntamiento);       
    }
    
    public static void ejercicio06(){        
        ayuntamiento.removeTransporte("1");
        ayuntamiento.removeTransporte("4");
        ayuntamiento.removeTransporte("7");
        System.out.println(ayuntamiento);       
    }
    
    private static void ejercicio07(){
        System.out.println("Dinero recaudado: " + ayuntamiento.getDineroRecaudado());
    }
    
    private static void ejercicio0809(){
        inicializar();
        
        ayuntamiento.setPrecioBilleteAutobus(3);
        ayuntamiento.setPrecioBilleteMetro(4.5);
        ejercicio01();
        System.out.println("Dinero recaudado: " + ayuntamiento.getDineroRecaudado());
        
    }
}
